# webpack-react-redux-es6-boilerplate ![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)

使用 webpack + react + redux + es6 的组件化前端项目 boilerplate。


> 开发时的环境

- [x] Mac OSX Yosemite
- [x] Node v6.3.0
- [x] NPM v3.10.3

> 快速开始

```
$ npm install
$ npm start
```

[ANT DESIGN](https://ant.design/index-cn)

![](https://t.alipayobjects.com/images/rmsweb/T1B9hfXcdvXXXXXXXX.svg)