// application's entry

import React, {Component} from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Router, Route, IndexRoute, browserHistory, Link} from 'react-router';
import reducers from 'reducers/index';

import 'antd/dist/antd.css';
import '../css/common.scss';
import {Layout, Menu, Breadcrumb, Icon} from 'antd';
const {Header, Content, Sider} = Layout;

// pages
import home from './home/index';
import login from './login/index';
import reg from './reg/index';

class Application extends Component {
    render() {
        return (
            <Layout>
                <Header className="header">
                    <div className="logo"/>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        // defaultSelectedKeys={['2']}
                        style={{lineHeight: '64px'}}
                    >
                        <Menu.Item key="1"><Link to="home">首页</Link></Menu.Item>
                        <Menu.Item key="2"><Link to="login">登录</Link></Menu.Item>
                        <Menu.Item key="3"><Link to="reg">注册</Link></Menu.Item>
                    </Menu>
                </Header>
                <div className="container">
                    {this.props.children}
                </div>
            </Layout>
        );
    }
}

const store = createStore(reducers, {}, applyMiddleware(thunk));

render((
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Application}>

                <IndexRoute component={home}/>
                <Route path="home" component={home}></Route>
                <Route path="login" component={login}></Route>
                <Route path="reg" component={reg}></Route>

            </Route>
        </Router>
    </Provider>
), document.getElementById('app'));